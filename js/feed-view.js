// Gets url history feed list
var listToArray = JSON.parse(Cookies.get('feedHistory'));
var latestFeed = listToArray[listToArray.length - 1];

// Google api feed initialization
google.load("feeds", "1");

// Gets specified url's feeds from Google and loads them into pageą
function initialize() {
    var feed = new google.feeds.Feed(latestFeed);
    feed.setNumEntries(10);

    feed.load(function(result) {
        if (!result.error) {
            var feedInfo = '';
            feedInfo += '<h1 class="f-h-name">'+result.feed.title+'</h1>';
            feedInfo += '<h3 class="f-h-description">'+result.feed.description+'</h3>';
            $('#feedHeader').append(feedInfo);
            for (var i = 0; i < result.feed.entries.length; i++) {
                var entry = result.feed.entries[i];
                var date = new Date(entry.publishedDate);
                var options = {
                    weekday: "long",
                    year: "numeric",
                    month: "short",
                    day: "numeric",
                    hour: "numeric",
                    minute: "numeric",
                    hour12: false
                };
                var day = date.toLocaleTimeString("en-En", options);
                var dateValue = date.valueOf();
                var content = '';
                var fixedContent = strip(entry.content);
                content += '<div class="feed">';
                content += '<div class="f-icon" feed-title="'+entry.title+'" feed-content="'+fixedContent+'" feed-link="'+entry.link+'" data-toggle="modal" data-target="#feedModal">';
                content += '<span class="glyphicon glyphicon-tag"></span>';
                content += '</div>';
                content += '<div class="f-content">';
                content += '<h2 class="f-title"><a href="#" feed-title="'+entry.title+'" feed-content="'+fixedContent+'" feed-link="'+entry.link+'" data-toggle="modal" data-target="#feedModal">'+entry.title+'</a></h2>';
                content += '<h5 date-value="'+dateValue+'" class="f-date">Published: '+day+'</h5>';
                content += '<p class="f-description">'+fixedContent+'</h5>';
                content += '</div>';
                content += '</div>';
                $('#feed').append(content);
                content = '';
            }
        } else {
            location.href = "error-page.html";
        }
    });
}

// Callback for Initialization on load
google.setOnLoadCallback(initialize);

// Sets or updates Feed url list, which is saved in Cookie
function setUrlFeedsList(newFeed) {
  listToArray = JSON.parse(Cookies.get('feedHistory'));
  if ($.inArray(newFeed, listToArray) === -1) {
    listToArray.push(newFeed);
    listStringify = JSON.stringify(listToArray);
    Cookies.set('feedHistory', listStringify, {expires: 7});
  } else {
    var target = $.inArray(newFeed, listToArray);
    var current = listToArray[target];
    listToArray.splice(target, 1);
    listToArray.push(current);
    listStringify = JSON.stringify(listToArray);
    Cookies.set('feedHistory', listStringify, {expires: 7});
  }
}

// Checks if there are any feeds in history
if (Cookies.get('feedHistory') !== undefined) {
    var listToArray = JSON.parse(Cookies.get('feedHistory'));
    for (i = listToArray.length - 1; i >= 0 ; i--) {
        $('#history').append('<a class="history-link" rss-data="'+listToArray[i]+'"></span>'+listToArray[i]+'</a>');
    };
}

$('.history-link').click(function() {
    var rss = $(this).attr('rss-data');
    setUrlFeedsList(rss);
    location.href = "feed-view.html";
});

// $('#feedId').click(function() {
//     var rss = $(this).val();
//     setUrlFeedsList(rss);
//     location.href = "feed-view.html";
// });

// Calls sortByName function on click, passes sorting direction
$('#sortName').click(function() {
    direction = $(this).attr('sorting');
    if (direction === 'z-a') {
        $(this).attr('sorting', 'a-z');
        $(this).find('.glyphicon').removeClass('glyphicon-sort-by-alphabet').addClass('glyphicon-sort-by-alphabet-alt');
    } else {
        $(this).attr('sorting', 'z-a');
        $(this).find('.glyphicon').removeClass('glyphicon-sort-by-alphabet-alt').addClass('glyphicon-sort-by-alphabet');
    }
    sortByName(direction);
    $('#sortDate').removeClass('active');
    $(this).addClass('active');
});

// Function which sorts feeds according the name, by using provided direction argument
function sortByName(direction) {
    var $feeds = $("#feed .feed");
    var sortedByName = $feeds.sort(function (a, b) {
        ab = $(a).find(".f-title").text().replace(/[&\/\\#,+()$~%.'"„“:*?<>{}]/g, ''),
        cd = $(b).find(".f-title").text().replace(/[&\/\\#,+()$~%.'"„“:*?<>{}]/g, '');
        if (direction === 'a-z') {
            return ab > cd;

        } else {
            return ab < cd;

        }
    });
    $("#feed").html(sortedByName);
};

// Calls sortByDate function on click, passes sorting direction
$('#sortDate').click(function() {
    direction = $(this).attr('sorting');
    if (direction === 'old-first') {
        $(this).attr('sorting', 'latest-first');
        $(this).find('.glyphicon').removeClass('glyphicon-sort-by-attributes-alt').addClass('glyphicon-sort-by-attributes');
    } else {
        $(this).attr('sorting', 'old-first');
        $(this).find('.glyphicon').removeClass('glyphicon-sort-by-attributes').addClass('glyphicon-sort-by-attributes-alt');
    }
    sortByDate(direction);
    $('#sortName').removeClass('active');
    $(this).addClass('active');
});

// Function which sorts feeds according the date, using provided direction argument
function sortByDate(direction) {
    var $feeds = $("#feed .feed");
    var sortedByDate = $feeds.sort(function (a, b) {
        ab = $(a).find(".f-date").attr('date-value'),
        cd = $(b).find(".f-date").attr('date-value');
        if (direction === 'old-first') {
            return ab > cd;
        } else {
            return ab < cd;
        }
    });
    $("#feed").html(sortedByDate);
};

var feedHeader = $('#header').html();
$("#header").load('header.html');
$("#modalWrapper").load('modal.html');

$(document).ready(function() {
    // Redirects to home page on #goBack button
    $('#goBack').click(function(){
        window.location.href = "index.html";
    });
    console.log(Cookies.get('feedHistory'));
    $('#header').append(feedHeader);

    $('#feedModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var title = button.attr('feed-title');
      var content = button.attr('feed-content');
      var link = button.attr('feed-link');
      var modal = $(this);
      modal.find('#feedTitle').text(title);
      modal.find('#feedContent').text(content);
      modal.find('#feedLink').attr('href', link);
    })
});
