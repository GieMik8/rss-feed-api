var formSuccess = false;

function strip(html)
{
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

// Gets entered feed url value and sets local storage, redirects to feed-view page
function sendFeed() {
  if (formSuccess === true) {
    var feedValue = $('#feedId').val();
    if (feedValue.indexOf("www.") !== 0) {
      feedValue = feedValue.replace('www.', '');
    }
    setUrlFeedsList(feedValue);
    location.href = "feed-view.html";
  }
}

$(document).ready(function(){
  var navHeight = $('.navbar').outerHeight();

  $(window).scroll(function() {
      if ($(this).scrollTop() > navHeight){
          $('body').addClass("sticked-nav");
      }
      else{
          $('body').removeClass("sticked-nav");
      }
  });

  $('#feedForm').validator({
    messages: {
      url: {
        required: "Enter your Email",
        url: 'Wrong url entered - maybe you forgot to type: "http://"',
      }
    }
  });

  $('#feedForm').on('validate.bs.validator', function(e) {

  });

  $('#feedForm').on('invalid.bs.validator', function(e) {
    console.log('invalid is here');
    formSuccess = false;
  });

  $('#feedForm').on('valid.bs.validator', function(e) {
    console.log('success');
    formSuccess = true;
  });

  $('#feedId').keypress(function(e) {
      if (e.which == '13' && formSuccess === true) {
          console.log('enter pressed and form success');
          sendFeed();
      }
  });
});
