// Sets or updates Feed url list, which is saved in Cookie
function setUrlFeedsList(newFeed) {
    if (Cookies.get('feedHistory') === undefined) {
        list = [];
        list.push(newFeed);
        listStringify = JSON.stringify(list);
        Cookies.set('feedHistory', listStringify, {expires: 7});
        console.log('nera');
    } else {
        listToArray = JSON.parse(Cookies.get('feedHistory'));

        if ($.inArray(newFeed, listToArray) === -1) {
            listToArray.push(newFeed);
            listStringify = JSON.stringify(listToArray);
            Cookies.set('feedHistory', listStringify, {expires: 7});
        } else {
            var target = $.inArray(newFeed, listToArray);
            var current = listToArray[target];
            listToArray.splice(target, 1);
            listToArray.push(current);
            listStringify = JSON.stringify(listToArray);
            Cookies.set('feedHistory', listStringify, {expires: 7});
        }
    }
}

// Initializes and adds event listener to send feed
(function(W){
    function init(){
        W.document.getElementById('sendIt').addEventListener('click', sendFeed, false);
    }

    W.addEventListener('load', init, false);
})(window);

var formSuccess = false;

$('#feedForm').validator({
  messages: {
    url: {
      required: "Enter your Email",
      url: 'Wrong url entered - maybe you forgot to type: "http://"',
    }
  }
});

$('#feedForm').on('validate.bs.validator', function(e) {

});

$('#feedForm').on('invalid.bs.validator', function(e) {
  console.log('invalid is here');
  formSuccess = false;
});

$('#feedForm').on('valid.bs.validator', function(e) {
  console.log('success');
  formSuccess = true;
});

function sendLastFeed() {
  location.href = "feed-view.html";
}

function lastFeed() {
  if (Cookies.get('feedHistory') !== undefined) {
    var listToArray = JSON.parse(Cookies.get('feedHistory'));
    var latestFeed = listToArray[listToArray.length - 1];
    $('.last-feed').append('<em>Looks like the last feed url you entered is: </em><a onclick="sendLastFeed()">'+latestFeed)+'</a>';
  }
}

$("#header").load('header.html');

$(document).ready(function() {
    console.log(Cookies.get('feedHistory'));
    lastFeed();
});

// TODO: refactor code
