### Project purpose ###

* This repository is based on a project named: "RSS Feed Reader". The main purpose of this 
* project is to test my JS skills.
* Version 0.7

### Setting up ###

* Pull the repository to your localhost folder (if using xampp: ":dir/xampp/htdocs/");

* Start your local web server. This api has been tested on Xampp;

* Note: In order to work this api needs a web server for creating, saving or editing cookies. All RSS history data is being kept in Cookies - at the present time it looks a best choice.

* To check this application type: "localhost/feedapi/" in your browser's address bar.

### Author comment ###

* This api is in development mode - there might be some missing features, some unexpected bugs might appear while using this application.